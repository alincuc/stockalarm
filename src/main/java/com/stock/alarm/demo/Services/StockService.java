package com.stock.alarm.demo.Services;


import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.concurrent.CompletableFuture;

@Service

public class StockService {


    private final RestTemplate restTemplate;

    public StockService(RestTemplateBuilder restTemplateBuilder) {
        this.restTemplate = restTemplateBuilder.build();
    }



    @Async
    public CompletableFuture<String> stockPrice(String index) throws InterruptedException {
        String url = String.format("https://www.alphavantage.co/query?function=GLOBAL_QUOTE&symbol=%s&apikey=98G7VK38G3VHQJA6", index);
        String results = restTemplate.getForObject(url, String.class);
        return CompletableFuture.completedFuture(results);
    }


}
