package com.stock.alarm.demo.Models;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import javax.persistence.*;
import java.util.Date;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Alarm {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column
    private double price;

    @Column
    Date createdDate = new Date();

    @Column
    String stockIndex;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User userAlarm;

}
