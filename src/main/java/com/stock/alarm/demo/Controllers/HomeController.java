package com.stock.alarm.demo.Controllers;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;



@Controller
public class HomeController {

    @GetMapping("/")
    private String homePage(Model model) {
        return "redirect:/alarm/alarm_list";
    }
}
