package com.stock.alarm.demo.Controllers;
import com.stock.alarm.demo.Models.Alarm;
import com.stock.alarm.demo.Services.StockService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

@Controller
@RequestMapping(value = "/alarm")
public class AlarmController {
    private final StockService stockService;
    public AlarmController(StockService stockService) {
        this.stockService = stockService;
    }

    @GetMapping("/")
    private String homePage() {
        return "redirect:/alarm/alarm_list";
    }

    @GetMapping(value = "/check_price")
    public String checkPrice(@RequestParam("index") String index, Model model) {

        CompletableFuture<String> page1 = null;
        try {
            page1 = stockService.stockPrice(index);
            CompletableFuture<String> page2 = stockService.stockPrice("APL");
            CompletableFuture<String> page3 = stockService.stockPrice("DOW");
            CompletableFuture.allOf(page1, page2, page3).join();
            model.addAttribute("response", page1.get());
            return "check_price";
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
            model.addAttribute("response", "Error");
            return "check_price";
        }
    }


    @GetMapping("/add") //load create page
    private String showAddAlarmPage(Model model) {
        model.addAttribute("alarm", new Alarm());
        return "alarm/add_alarm";
    }

    @GetMapping("/alarm_list") //read
    private String alarmList() {
        return "alarm/alarm_list";
    }



}
